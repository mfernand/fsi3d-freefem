#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <set>
#include <map>
#include <vector>
#include <math.h>   

 using namespace std;

int main(int argc, char ** argv)
{

  cout << " Input FSI mesh?" <<endl;
  string fsifile;
  cin >> fsifile;
  cout << " Output mesh?" <<endl;
  string outfile;
  cin >> outfile;
  cout << " Subdomain labe to extract?" << endl;
  int label;
  cin >> label;
  
  ifstream inFile(fsifile);
  string line;
  getline(inFile,line);
  getline(inFile,line);
  getline(inFile,line); 
  getline(inFile,line);
  int nV;
  inFile >> nV;
  std::cout << nV << std::endl;
		     
		     vector< vector<double> > coor(nV);
		     vector<int> vref(nV);
		     for (int i=0; i <nV; ++i) {
		       coor[i].reserve(3);
		       inFile >> coor[i][0] >> coor[i][1] >> coor[i][2] >> vref[i] ;
		       cout << coor[i][0] << " "  << coor[i][1] << " " << coor[i][2]<< " " << vref[i] << endl;
		     }
		     getline(inFile,line);
		        cout << line <<endl;
		       getline(inFile,line);
		       cout << line <<endl;
		       int nT;
		       inFile >> nT;
		       std::cout << nT << std::endl ;
		       vector< vector<int> > tetra(nT);
		       for (int i=0; i <nT; ++i) {
			 tetra[i].reserve(5);
			 for (int j=0; j<5;++j) {
			   inFile >>  tetra[i][j];
			   cout << tetra[i][j] << " ";
			 }
			 cout << endl;
		       }
		       getline(inFile,line);
		       cout << line <<endl;
		       getline(inFile,line);
		       cout << line <<endl;
		       int nTria;
		       inFile >> nTria;
		        std::cout << nTria << std::endl ;
			 vector< vector<int> > tria(nTria);
		       for (int i=0; i <nTria; ++i) {
			 tria[i].reserve(4);
			 for (int j=0; j<4;++j) {
			   inFile >>  tria[i][j];
			   cout << tria[i][j] << " ";
			 }
			 cout << endl;
		       }
		       inFile.close();

		       
		       vector<int> ifP(nV,0);
		       vector<int> ifT(nT,0);
		       for (int i=0; i <nT; ++i) {
			 if ( tetra[i][4] == label ) {
			   ifT[i] =1;
			   for (int j=0; j<4; j++)
			     ifP[  tetra[i][j]-1 ] = 1;
			 }
		       }

		       int count=0;
		       for (int i=0; i < nV; ++i) {
			 if (ifP[i] == 1 )
			   count++;
		       }
		       int nVN = count;
		       count=0;
		       for (int i=0; i < nT; ++i) {
			 if (ifT[i] == 1 )
			   count++;
		       }
		       int nTN = count;

		       cout << nVN << "  " << nTN << endl;
		       vector< vector<double> > coorN(nVN);
						     vector< int> vP(nV,-1);			     
		       vector<int> vrefN;
						     count = -1;
                       for (int i=0; i < nV; ++i) {
			 if (ifP[i] == 1 ) {
			   count++;
			   coorN[count].reserve(3);
			   for(int j=0; j<3; j++)
			     coorN[count][j] = coor[i][j];
			   vP[i] = count+1;
			   vrefN.push_back(vref[i]);
			 }
		       }
			 vector< vector<int> > tetraN(nTN);
			 count = -1;
		       for (int i=0; i < nT; ++i) {
			 if (ifT[i] == 1 ) {
			   count++;
			   tetraN[count].reserve(5);
			   for(int j=0; j<4; j++)
			     tetraN[count][j] = vP[tetra[i][j]-1];
			   tetraN[count][4] = tetra[i][4];
			 }
		       }

			 
			 count = 0;
			 for (int i=0; i < nTria; ++i) {

			   if ( (ifP[tria[i][0]-1] == 1 ) and (ifP[tria[i][1]-1] == 1 ) and (ifP[tria[i][2]-1] == 1 ) ) {
			     count++;
			   }
			 }
			 int nTriaN = count;
			 cout << nTriaN <<endl;
					  vector< vector<int> > triaN(nTriaN);
					  count = -1;
			 for (int i=0; i < nTria; ++i) {

			   if ( (ifP[tria[i][0]-1] == 1 ) and (ifP[tria[i][1]-1] == 1 ) and (ifP[tria[i][2]-1] == 1 ) ) {
			     count++;
			     vector<int> tmp(4);
			     for (int j=0; j<3; j++)
			       tmp[j] = vP[tria[i][j]-1]; 
			     tmp[3] = tria[i][3];
			     triaN[count].reserve(4);
			     for(int j=0; j<4; j++)
			       triaN[count][j] = tmp[j];
			   }
			 }
					      
			
			
  FILE * pFile = fopen (outfile.c_str(),"w+");
  fprintf(pFile,"MeshVersionFormatted 1\n");  
  fprintf(pFile,"Dimension\n");
  fprintf(pFile,"3\n");
  fprintf(pFile,"Vertices\n");
  fprintf(pFile,"%8d\n",nVN);
						
  for (int i=0; i< nVN; i++) {
    //cout << coorN[i][0] << " "  << coorN[i][1] << " " << coorN[i][2]<< endl;
    fprintf( pFile, "%12.5e%12.5e%12.5e%8d\n", coorN[i][0],coorN[i][1],coorN[i][2],vrefN[i]);
  }
			 			  cout << "hola" <<endl;
  string keyword = "Tetrahedra";
  fprintf( pFile, "%s\n", keyword.c_str() );
  fprintf(pFile,"%8d\n", nTN);
  for (int i=0; i< nTN; i++)
    fprintf( pFile, "%8d%8d%8d%8d%8d\n",tetraN[i][0],tetraN[i][1],tetraN[i][2],tetraN[i][3],tetraN[i][4]);

			 keyword = "Triangles";
  fprintf( pFile, "%s\n", keyword.c_str() );
  fprintf(pFile,"%8d\n", nTriaN);
  for (int i=0; i< nTriaN; i++)
    fprintf( pFile, "%8d%8d%8d%8d\n",triaN[i][0],triaN[i][1],triaN[i][2],triaN[i][3]);
			 
  fprintf(pFile,"End"); 
  fclose(pFile);
		       
		       
}

