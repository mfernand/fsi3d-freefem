load "msh3"
load "ensight3d"
load "MUMPS"
//------------------
// Free user paprameters 
//------------------
real gamma   = 100; // Robin coefficient 
int cn = 1; // solid cn
int display = 1;   // 1= plot, 0=no plot 
real coef = 1;      // amplification factor of the displacement (for display purposes) 
//---------------------
// discretization
//---------------------
real dt   = 1e-3;
real gammap     = 0.05;

//--------------------------------------
//fluid phisical quantities
//--------------------------------------
real rhof       = 1.; //density fluid
real mu         = 0.035; // fluid viscosity
real Press      = 2.0E4; // inlet pressure
real Tstar      = 0.005;
real Tfinal     = 1.5;
real pmax = 1000;

//-------------------------
// solid phisical quantities 
//-------------------------
real rhos    = 1.1;   //density structure
real E = 1e6;
real nu = 0.3;
real lambda1 = E/(2.0*(1.0 + nu) );
real lambda2 = E*nu/( (1.0 + nu) * (1.0-2.0*nu ) );

real sqrt2=sqrt(2.);
macro dn(u) ( N.x*dx(u) + N.y*dy(u) + N.z*dz(u) ) //
macro epsilon(u1,u2,u3)  [dx(u1),dy(u2),dz(u3),(dz(u2)+dy(u3))/sqrt2,(dz(u1)+dx(u3))/sqrt2,(dy(u1)+dx(u2))/sqrt2] // EOM
macro epsilon11(u1,u2,u3)  [dx(u1),(dz(u1)+dx(u3))/sqrt2,(dy(u1)+dx(u2))/sqrt2] // EOM
macro epsilon1(u1)  [dx(u1),dz(u1)/sqrt2,dy(u1)/sqrt2] // EOM
macro epsilon22(u1,u2,u3)  [dy(u2),(dz(u2)+dy(u3))/sqrt2,(dy(u1)+dx(u2))/sqrt2] // EOM
macro epsilon2(u2)  [dy(u2),dz(u2)/sqrt2,dx(u2)/sqrt2] // EOM
macro epsilon33(u1,u2,u3)  [dz(u3),(dz(u2)+dy(u3))/sqrt2,(dz(u1)+dx(u3))/sqrt2] // EOM
macro epsilon3(u3)  [dz(u3),dy(u3)/sqrt2,dx(u3)/sqrt2] // EOM

  macro div(u1,u2,u3) ( dx(u1)+dy(u2)+dz(u3) ) // EOM
//--------------------------------------

real tau  = 1.0/dt;
real Nstep = Tfinal/dt;


real t = 0.0;


  

//mesh3 Th=cube(20,20,20);
mesh3 Thf, Ths;
Thf=readmesh3("fluid.mesh");
Ths=readmesh3("solid.mesh");
//plot(Thf,wait=1);

//---- 

 //---- Save mesh in ensight format

{
  mesh3 Th=Ths;
  ofstream outmesh("./Post/meshs");
  outmesh << Th.nv << endl;
  outmesh << Th.nt << endl;
  outmesh << Th.nbe << endl;
  for(int i=0; i < Th.nv; ++i) 
    outmesh <<  Th(i).x << "  " << Th(i).y << "  "<< Th(i).z << endl;

  for(int k=0; k < Th.nt; ++k) 
    outmesh << int(Th[k][0]) << " " << int(Th[k][1]) << " " << int(Th[k][2])  << "  " <<  int(Th[k][3])  << "  " << Th[k].label << endl;

  for (int k=0;k<Th.nbe;++k)
    outmesh << Th.be(k)[0] << " " << Th.be(k)[1]<< " " << Th.be(k)[2]<< " " << Th.be(k).label <<  endl;

 }

writems(0);
//---- 

//---- Save mesh in ensight format

{
  mesh3 Th=Thf;
  ofstream outmesh("./Post/mesh");
  outmesh << Th.nv << endl;
  outmesh << Th.nt << endl;
  outmesh << Th.nbe << endl;
  for(int i=0; i < Th.nv; ++i) 
    outmesh <<  Th(i).x << "  " << Th(i).y << "  "<< Th(i).z << endl;

  for(int k=0; k < Th.nt; ++k) 
    outmesh << int(Th[k][0]) << " " << int(Th[k][1]) << " " << int(Th[k][2])  << "  " <<  int(Th[k][3])  << "  " << Th[k].label << endl;

  for (int k=0;k<Th.nbe;++k)
    outmesh << Th.be(k)[0] << " " << Th.be(k)[1]<< " " << Th.be(k)[2]<< " " << Th.be(k).label <<  endl;

 }

writem(0);




fespace Xhs(Ths,P1);
fespace Xhf(Thf,P1);
fespace Qhf(Thf,P1);
fespace Vhf(Thf,[P1,P1,P1,P1]);
fespace Vhs(Ths,[P1,P1,P1]);


Xhf u1,u2,u3, v1,v2,v3, u1old, u2old, u3old, etaFx,etaFy, etaFz;
Qhf pold, p,q, pext,mp;

Xhs wx,wy,wz, etax,etay,etaz, vsx,vsy,vsz, tx,ty,wxold,wyold,wzold,
  wxold1,wyold1,wzold1,etaxold,etayold,etazold, aux1, aux2, aux3;



u1old = 0;
u2old = 0;
u3old = 0.;
pold=0;
etaxold = 0;
etayold = 0;
etazold = 0;
wxold =  0.0;
wyold  =  0.0;
wzold = 0.0;
wxold1 = 0.0;
wyold1 = 0.0;
wzold1 = 0.0;


// matrix fluid 

varf fluidvar ([u1,u2,u3,p],[v1,v2,v3,q]) =

int3d(Thf)( 
	   rhof*tau*(u1*v1 + u2*v2 + u3*v3)  + 2.*mu*( epsilon(u1,u2,u3)'*epsilon(v1,v2,v3)  ) - p*div(v1,v2,v3) + div(u1,u2,u3)*q
)

// stabilization
  +int3d(Thf)( gammap*(volume^(2./3.)/mu)*(dx(p)*dx(q) + dy(p)*dy(q) + dz(p)*dz(q)) )

// coupling terms
+ int2d(Thf,10)( (gamma*mu/hTriangle)*( u1*v1 + u2*v2 + u3*v3 ) )

-int2d(Thf,10)( q * ( N.x * u1 + N.y * u2 + N.z*u3) )


- int2d(Thf,10)( 2.*mu*(    (dx(u1)*N.x + (dx(u2) + dy(u1))*.5*N.y + (dx(u3) + dz(u1))*.5*N.z  )*v1
			 + ((dx(u2) + dy(u1))*.5*N.x + dy(u2)*N.y  + (dy(u3) + dz(u2))*.5*N.z  )*v2
			 + ((dx(u3) + dz(u1))*.5*N.x + (dy(u3) + dz(u2))*.5*N.y  + dz(u3)*N.z  )*v3 )
		 - p* ( N.x * v1 + N.y * v2 + N.z * v3) )

  + on(20,u3=0) + on(1,3,u1=0.,u2=0.,u3=0.);




// rhs fluid 

varf rhsfv1(unused,v1) = int3d(Thf)(rhof*tau*u1old*v1)  - int2d(Thf,10)( (gamma*mu/hTriangle)*tau*etaxold*v1 )
  + int2d(Thf,4)( -pmax*sin(8*pi*t/Tfinal)*v1*N.x   )  ;

varf rhsfv2(unused,v2) = int3d(Thf)(rhof*tau*u2old*v2) - int2d(Thf,10)( (gamma*mu/hTriangle)*tau*etayold*v2 ) ;

varf rhsfv3(unused,v3) = int3d(Thf)(rhof*tau*u3old*v3) - int2d(Thf,10)( (gamma*mu/hTriangle)*tau*etazold*v3 ) ;

varf rhsfq(unused,q) =  int2d(Thf,10)( tau* q * ( N.x * etaxold + N.y * etayold + N.z * etazold) )  ;



//m matrix solid 

varf solidvar([etax,etay,etaz],[vsx,vsy,vsz]) =
  int3d(Ths)( (1.+cn)*rhos * (tau^2)* ( etax * vsx + etay * vsy +  etaz * vsz )
	      + (1./(1.+cn))*(2.0*lambda1*( epsilon(etax,etay,etaz)'*epsilon(vsx,vsy,vsz) ) +
			lambda2*div(etax,etay,etaz)*div(vsx,vsy,vsz) )		  )

  + int2d(Ths,10)(  (gamma*mu/hTriangle)*tau*(etax*vsx + etay*vsy + etaz*vsz)  )
  + on(1,etax=0,etay=0,etaz=0) + on(20,etaz=0);

// rhs solid 

varf  rhssx(unused,vsx) =  int3d(Ths)( (1.+cn)*(rhos*tau*wxold*vsx + rhos*(tau^2)*etaxold*vsx ))
+ int2d(Ths,10)( (gamma*mu/hTriangle)*tau*etaxold*vsx )
  - int3d(Ths)(  (1./(1.+cn))*(2.0*lambda1*( epsilon11(etaxold,etayold,etazold)'*epsilon1(vsx) ) +
			lambda2*div(etaxold,etayold,etazold)*dx(vsx) )); 

varf  rhssy(unused,vsy) =  int3d(Ths)( (1.+cn)*(rhos*tau*wyold*vsy + rhos*(tau^2)*etayold*vsy )) 
+ int2d(Ths,10)( (gamma*mu/hTriangle)*tau*etayold*vsy )
- int3d(Ths)(  (1./(1.+cn))*(2.0*lambda1*(  epsilon22(etaxold,etayold,etazold)'*epsilon2(vsy) )
			     + lambda2*div(etaxold,etayold,etazold)*dy(vsy)));

varf  rhssz(unused,vsz) =  int3d(Ths)( (1.+cn)*(rhos*tau*wzold*vsz + rhos*(tau^2)*etazold*vsz )) 
+ int2d(Ths,10)( (gamma*mu/hTriangle)*tau*etazold*vsz )
- int3d(Ths)(  (1./(1.+cn))*(2.0*lambda1*(  epsilon33(etaxold,etayold,etazold)'*epsilon3(vsz) )
			     + lambda2*div(etaxold,etayold,etazold)*dz(vsz)));


  // cross terms
varf   fs ([u1,u2,u3],[v1,v2,v3,q]) = int2d(Thf,10)( tau* q * ( N.x * u1 + N.y * u2 + N.z * u3) )
  - int2d(Thf,10)( tau*(gamma*mu/hTriangle)*( u1*v1 + u2*v2 + u3*v3 ) );

varf   sf ([u1,u2,u3,p],[v1,v2,v3]) =  - int2d(Thf,10)( (gamma*mu/hTriangle)*( u1*v1 + u2*v2 + u3*v3 ) )
  + int2d(Thf,10)( 2.*mu*(    (dx(u1)*N.x + (dx(u2) + dy(u1))*.5*N.y + (dx(u3) + dz(u1))*.5*N.z  )*v1
			   + ((dx(u2) + dy(u1))*.5*N.x + dy(u2)*N.y  + (dy(u3) + dz(u2))*.5*N.z  )*v2
			   + ((dx(u3) + dz(u1))*.5*N.x + (dy(u3) + dz(u2))*.5*N.y  + dz(u3)*N.z  )*v3 )
		   - p* ( N.x * v1 + N.y * v2 + N.z * v3 ) );


matrix Af  = fluidvar(Vhf,Vhf);
matrix As  = solidvar(Vhs,Vhs);
matrix FS  = fs(Vhs,Vhf);
matrix SF  = sf(Vhf,Vhs);


cout << " udating full matrix.... " ;

matrix Afsi = [ [ Af  , FS],
		[ SF , As] ];

 

set(Afsi,solver=sparsesolver);

cout << "done." << endl;


int ndoft = Vhf.ndof+Vhs.ndof;

real [int] rhs(ndoft), sol(ndoft), auxf1(Xhf.ndof), auxf2(Xhf.ndof), auxf3(Xhf.ndof), auxfq(Xhf.ndof),
  auxsx(Xhs.ndof), auxsy(Xhs.ndof), auxsz(Xhs.ndof);
Xhf vm;


for(int m=0; m< Nstep; m++)
{	

	t += dt;

	cout << " ###########################################\n";     
	cout << " We are at time: " << t << "\n";
	cout << " ###########################################\n";    

	rhs = 0.;

	// rhs fluid
	auxf1 = rhsfv1(0,Xhf);
	auxf2 = rhsfv2(0,Xhf);
	auxf3 = rhsfv3(0,Xhf);
	auxfq = rhsfq(0,Xhf);
	for (int i=0; i< Xhf.ndof; ++i){
	  rhs[4*i]   += auxf1[i];
	  rhs[4*i+1] += auxf2[i];
	  rhs[4*i+2] += auxf3[i];
	  rhs[4*i+3] += auxfq[i];
	}
	
	// rhs solid
	auxsx = rhssx(0,Xhs);
	auxsy = rhssy(0,Xhs);
	auxsz = rhssz(0,Xhs);

	for (int i=0; i< Xhs.ndof; ++i){
	  rhs[Vhf.ndof+3*i]   += auxsx[i];
	  rhs[Vhf.ndof+3*i+1] += auxsy[i];
	  rhs[Vhf.ndof+3*i+2] += auxsz[i];
	}


	// solving linear system 
	sol = Afsi^-1*rhs;
	
	// saving solution fluid
	for (int i=0; i< Xhf.ndof; ++i){
	  u1[][i] = sol[4*i] ;
	  u2[][i] = sol[4*i+1] ;
	  u3[][i] = sol[4*i+2] ; 
	  p[][i]  = sol[4*i+3] ;
	}


	for (int i=0; i< Xhs.ndof; ++i){
	  etax[][i] = sol[Vhf.ndof+3*i];
	  etay[][i] = sol[Vhf.ndof+3*i+1];
	  etaz[][i] = sol[Vhf.ndof+3*i+2];
	}

	//cout  << etay[] << endl;

	wx = (1.+cn)*tau*(etax - etaxold) -cn*wxold;
	wy = (1.+cn)*tau*(etay - etayold) -cn*wyold;
	wz = (1.+cn)*tau*(etaz - etazold) -cn*wzold;
	
	//mesh3 Thsm=movemesh(Ths,[x+coef*etax,y+coef*etay,z+coef*etaz]);	
	//plot(Thsm,Thf,etax,p,fill=true,value=1, cmm=("time = "+t ),wait=1 );
	
	
	etaxold = etax;
	etayold = etay;
	etazold = etaz;
	wxold1 = wxold;
	wyold1 = wyold;
	wzold1 = wzold;
	wxold = wx;
	wyold = wy;
	wzold = wz;
	u1old=u1;
	u2old=u2;
	u3old=u3;


	// --- write solution in array format 	
	{
	  ofstream outp("./Post/p");
	  outp << p[].n << endl;
	  for (int i=0; i < p[].n; ++i)
	    outp << p[][i] << endl;
	  ofstream outu1("./Post/u1");
	  outu1 << u1[].n << endl;
	  for (int i=0; i < u1[].n; ++i)
	    outu1 << u1[][i] << endl;
	  ofstream outu2("./Post/u2");
	  outu2 << u2[].n << endl;
	  for (int i=0; i < u2[].n; ++i)
	    outu2 << u2[][i] << endl;
	  ofstream outu3("./Post/u3");
	  outu3 << u3[].n << endl;
	  for (int i=0; i < u3[].n; ++i)
	    outu3 << u3[][i] << endl;

	  ofstream outdep1("./Post/dep1");
	  outdep1 << etax[].n << endl;
	  for (int i=0; i < etax[].n; ++i)
	    outdep1 << etax[][i] << endl;
	  ofstream outdep2("./Post/dep2");
	  outdep2 << etay[].n << endl;
	  for (int i=0; i < etay[].n; ++i)
	    outdep2 << etay[][i] << endl;
	  ofstream outdep3("./Post/dep3");
	  outdep3 << etaz[].n << endl;
	  for (int i=0; i < etaz[].n; ++i)
	    outdep3 << etaz[][i] << endl;
	}
// --- write ensight formats 
	writec(t); // case  file 
	writeu(m); // velocity  file 
	writep(m); // prssure file
	writecs(t); // case  file 
	writed(m);
 }
