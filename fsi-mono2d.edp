load "MUMPS"
//------------------
// Free user paprameters 
//------------------
real gamma   = 100; // Robin coefficient 
int cn = 1; // solid cn
int display = 1;   // 1= plot, 0=no plot 
real coef = 0;      // amplification factor of the displacement (for display purposes) 
//---------------------
// discretization
//---------------------
real h    = 0.1/2;
real dt   = 1e-4;
real gammap     = 0.001;

//--------------------------------------
//fluid phisical quantities
//--------------------------------------
real rhof       = 1; //density fluid
real mu         = 0.035; // fluid viscosity
real Press      = 2.0E4; // inlet pressure
real Tstar      = 0.005;
real Tfinal     = 0.015;


//-------------------------
// solid phisical quantities 
//-------------------------
real rhos    = 1.1;   //density structure
real lambda1 = 1.15e6; //E/(2.0*(1.0 + nu) );
real lambda2 = 1.7e6;  // E*nu/( (1.0 + nu) * (1.0-2.0*nu ) );
real beta    = 4.e6;


macro dn(u) ( N.x*dx(u) + N.y*dy(u) )
//--------------------------------------

real L    = 6; // length of the fluid domain
real R = 0.5; // radius of the fluid domain 
real eps = 0.1; // thickness of the solid  domain

// meshes 
int  nx     = L/h;
int  ny     = R/h;
int  nys    = eps/h;

mesh Thf=square(nx,ny,[0+L*x,0+R*y]);
mesh Ths=square(nx,nys, [0+L*x,R+eps*y]);

//plot(Thf,Ths);

int nbvf = Thf.nv;
real [int] matchf(nx+1);
int  count = 0;
for (int i=0; i<nbvf; ++i)
{
  if ( abs(Thf(i).y - R) < 1e-8 )
        {
                matchf[count] = i;
                ++count;
        }
}

int nbvs = Ths.nv;
real [int] matchs(nx+1);
count = 0;
for (int i=0; i<nbvs; ++i)
{
        if ( abs(Ths(i).y - R) < 1e-8 )
        {
                matchs[count] = i;
                ++count;
        }
}


int nbintf = matchf.n, nbints = matchs.n, nbint;

if (   nbintf != nbintf ) {
   cout << " Interface matching error " <<endl;
   exit(1);
}
else
   nbint = nbintf;
real [int] xcs(nbint), xcf(nbint);
// cout << "============== MATCH =============" << endl;
for (int i=0; i<nbint; i++) {
    xcf[i] = Thf(matchf[i]).x;
    xcs[i] = Ths(matchs[i]).x;
  
    //   cout << matchf[i] <<  " " << matchs[i] << endl;
    if ( abs(xcf[i] - xcs[i]) > 1e-8 ) exit(1);
 }
//  cout << "==================================" << endl;

real tau  = 1.0/dt;
real Nstep = Tfinal/dt;


real t = 0.0;

fespace Xhs(Ths,P1);
fespace Xhf(Thf,P1);
fespace Qhf(Thf,P1);
fespace Vhf(Thf,[P1,P1,P1]);
fespace Vhf12(Thf,[P1,P1]);
fespace Vhs(Ths,[P1,P1]);

Xhs wx,wy,etax,etay,vsx,vsy,tx,ty,wxold,wyold,wxold1,wyold1,etaxold,etayold, aux1, aux2;

Xhf u1,u2,v1,v2, u1old, u2old, etaFx,etaFy;
Qhf pold, p,q, pext,mp;

etaxold = 0;
etayold = 0;
wxold =  0.0;
wyold  =  0.0; 
wxold1 = 0.0;
wyold1 = 0.0;
u1old = 0;
u2old = 0;
pold=0;


//for (int i=0; i< Ths.nv; i++) 
//  etayold[][i]= sin(Thf(i).x*acos(-1)/L)*0.01;
   
macro e11(u,v) dx(u) // 
macro e22(u,v) dy(v) // 
macro e12(u,v) ( (dx(v) + dy(u))*.5) // 
macro div(u,v) ( dx(u)+dy(v) ) //

varf fluidvar ([u1,u2,p],[v1,v2,q]) =

int2d(Thf)( 
rhof*tau*(u1*v1 + u2*v2)  + 2.*mu*(  e11(u1,u2)*e11(v1,v2) +  e22(u1,u2)*e22(v1,v2)  + 2.*e12(u1,u2)*e12(v1,v2)   ) - p*div(v1,v2) + div(u1,u2)*q
)

// stabilization
+int2d(Thf)( gammap*(hTriangle^2/mu)*(dx(p)*dx(q) + dy(p)*dy(q)) )

// coupling terms
+ int1d(Thf,3)( (gamma*mu/lenEdge)*( u1*v1 + u2*v2 ) )

-int1d(Thf,3)( q * ( N.x * u1 + N.y * u2) )


- int1d(Thf,3)( 2.*mu*( (e11(u1,u2)*N.x + e12(u1,u2)*N.y)*v1 + (e12(u1,u2)*N.x + e22(u1,u2)*N.y)*v2 ) - p* ( N.x * v1 + N.y * v2 ) )

+ on(1,u2=0.0);


varf rhsfv1(unused,v1) = int2d(Thf)(rhof*tau*u1old*v1)
  + int1d(Thf,4)(0.5*Press*(1-cos(2.*t*pi/Tstar))*v1   )
  - int1d(Thf,3)( (gamma*mu/lenEdge)*tau*etaxold*v1 ) ;
varf rhsfv2(unused,v2) = int2d(Thf)(rhof*tau*u2old*v2)
  - int1d(Thf,3)( (gamma*mu/lenEdge)*tau*etayold*v2 ) ;
varf rhsfq(unused,q) =  int1d(Thf,3)( tau* q * ( N.x * etaxold + N.y * etayold) )  ;



varf solidvar([etax,etay],[vsx,vsy]) =
  int2d(Ths)( (1.+cn)*rhos * (tau^2)* ( etax * vsx + etay * vsy )
        + (1./(1.+cn))*(2.0*lambda1*( e11(etax,etay)*e11(vsx,vsy)+ e22(etax,etay)*e22(vsx,vsy)
       + 2.*e12(etax,etay)*e12(vsx,vsy) ) + lambda2*div(etax,etay)*div(vsx,vsy) + beta*( etax * vsx + etay * vsy ))		  )
+ int1d(Ths,1)(  (gamma*mu/lenEdge)*tau*(etax*vsx + etay*vsy)  )
+ on(2,4,etax=0,etay=0);

varf  rhssx(unused,vsx) =  int2d(Ths)( (1.+cn)*(rhos*tau*wxold*vsx + rhos*(tau^2)*etaxold*vsx ))
+ int1d(Ths,1)( (gamma*mu/lenEdge)*tau*etaxold*vsx )
- int2d(Ths)(  (1./(1.+cn))*(2.0*lambda1*( e11(etaxold,etayold)*dx(vsx)+ 
					   + e12(etaxold,etayold)*dy(vsx) ) + lambda2*div(etaxold,etayold)*dx(vsx) +
			     beta*( etaxold * vsx )));
  ; 
 

varf  rhssy(unused,vsy) =  int2d(Ths)( (1.+cn)*(rhos*tau*wyold*vsy + rhos*(tau^2)*etayold*vsy )) 
+ int1d(Ths,1)( (gamma*mu/lenEdge)*tau*etayold*vsy )
- int2d(Ths)(  (1./(1.+cn))*(2.0*lambda1*(e22(etaxold,etayold)*dy(vsy)
	     + e12(etaxold,etayold)*dx(vsy) ) + lambda2*div(etaxold,etayold)*dy(vsy) +
	       beta*(  etayold * vsy )));

  ;



  // cross terms
varf   fs ([u1,u2],[v1,v2,q]) = int1d(Thf,3)( tau* q * ( N.x * u1 + N.y * u2) )
  - int1d(Thf,3)( tau*(gamma*mu/lenEdge)*( u1*v1 + u2*v2 ) );




varf   sf ([u1,u2,p],[v1,v2]) =
  - int1d(Thf,3)( (gamma*mu/lenEdge)*( u1*v1 + u2*v2 ) )+
 int1d(Thf,3)( 2.*mu*( (e11(u1,u2)*N.x + e12(u1,u2)*N.y)*v1 + (e12(u1,u2)*N.x + e22(u1,u2)*N.y)*v2 )
		- p* ( N.x * v1 + N.y * v2 ) );


matrix Af      = fluidvar(Vhf,Vhf);
matrix As      = solidvar(Vhs,Vhs);
matrix FS      = fs(Vhs,Vhf);
matrix SF      = sf(Vhf,Vhs);



 // real [int,int] Asf(Vhs.ndof,Vhf.ndof), Afs(Vhf.ndof,Vhs.ndof);
//  Asf = 0;
//  Afs = 0;

// cout << " udating cross matrix blocks.... " ;

// // Sparse matrix set
// int[int] IFS(1), JFS(1);
// real[int] CFS(1);



// [IFS, JFS, CFS] = FS;

// //cout << CFS << endl;



// int[int] find(IFS.n);
// find = 1;
// count=0;
// for (int l=0; l< IFS.n; ++l) {
//   if ( abs ( CFS(l) ) < 1e-16 ) {
//     find[l] = 0;
//     count++;
//   }
//  }

// int[int] IFSC(IFS.n-count), JFSC(IFS.n-count);
// real[int] CFSC(IFS.n-count);
// int it=-1;
// for (int l=0; l< IFS.n; ++l) {
//   if ( find[l] != 0 ) {
//     it++; 
//     IFSC[it] = IFS[l];
//     JFSC[it] = JFS[l];
//     CFSC[it] = CFS[l];
//   }
//  }

// //cout << CFSC << endl;
// //cout << CFS.n << " " << CFSC.n << endl;

// FS = [IFSC,JFSC,CFSC];
 
// for (int l=0; l< IFSC.n; ++l) {
//   int i = IFSC[l];
//   for (int j=0; j< nbint; ++j) {
//     Afs(i,2*matchs[j])   = FS (i,2*matchf[j]);
//     Afs(i,2*matchs[j]+1) = FS (i,2*matchf[j]+1);
//   }
//  }


// int[int] ISF(1), JSF(1);
// real[int] CSF(1);
 
// [ISF, JSF, CSF] = SF;
// find.resize(ISF.n);
// find = 1;
// count=0;
// for (int l=0; l< ISF.n; ++l) {
//   if ( abs ( CSF(l) ) < 1e-16 ) {
//     find[l] = 0;
//     count++;
//   }
//  }

// int[int] ISFC(ISF.n-count), JSFC(ISF.n-count);
// real[int] CSFC(ISF.n-count);
// it=-1;
// for (int l=0; l< ISF.n; ++l) {
//   if ( find[l] != 0 ) {
//     it++; 
//     ISFC[it] = ISF[l];
//     JSFC[it] = JSF[l];
//     CSFC[it] = CSF[l];
//   }
//  }

// SF = [ISFC,JSFC,CSFC];

 
// for (int l=0; l< JSFC.n; ++l) {
//   int j=JSFC[l];
//   for (int i=0; i< nbint; ++i) 
//     {
//       Asf(2*matchs[i],j)     = SF (2*matchf[i],j);
//       Asf(2*matchs[i]+1,j)   = SF (2*matchf[i]+1,j);
//     }
//  }

// cout << "done." << endl;  
// //exit(1);
// //cout << Afs << endl;
// //cout << Asf << endl;

// matrix ASF = Asf;
// matrix AFS = Afs;


cout << " udating full matrix.... " ;

matrix Afsi = [ [ Af  , FS],
		[ SF , As] ];

 

set(Afsi,solver=sparsesolver);

cout << "done." << endl;

int ndoft = Vhf.ndof+Vhs.ndof;

real [int] rhs(ndoft), sol(ndoft), auxf1(Xhf.ndof), auxf2(Xhf.ndof), auxfq(Xhf.ndof), auxsx(Xhs.ndof), auxsy(Xhs.ndof);

//********************
problem lift([etaFx,etaFy],[v1,v2],init=1) = int2d(Thf)( 
dx(etaFx)*dx(v1) + dy(etaFx)*dy(v1) + dx(etaFy)*dx(v2) + dy(etaFy)*dy(v2) ) 
+on(1,2,4,etaFx=0,etaFy=0) + on(3,etaFx=etax,etaFy=etay); 



for(int m=0; m< Nstep; m++)
{	

	t += dt;
	
	if ( t > Tstar )
	     Press = 0.0;
	
	cout << " ###########################################\n";     
	cout << " We are at time: " << t << "\n";
	cout << " ###########################################\n";     

	rhs = 0.;

	// rhs fluid
	auxf1 = rhsfv1(0,Xhf);
	auxf2 = rhsfv2(0,Xhf);
	auxfq = rhsfq(0,Xhf);
	
	for (int i=0; i< Xhf.ndof; ++i){
	  rhs[3*i] += auxf1[i];
	  rhs[3*i+1] += auxf2[i];
	  rhs[3*i+2] += auxfq[i];
	}

	// rhs solid
	auxsx = rhssx(0,Xhs);
	auxsy = rhssy(0,Xhs);

	for (int i=0; i< Xhs.ndof; ++i){
	  rhs[Vhf.ndof+2*i]   += auxsx[i];
	  rhs[Vhf.ndof+2*i+1] += auxsy[i];
	}


	// solving linear system 
	sol = Afsi^-1*rhs;
	
	// saving solution fluid
	for (int i=0; i< Xhf.ndof; ++i){
	  u1[][i] = sol[3*i] ;
	  u2[][i] = sol[3*i+1] ;  
	  p[][i]  = sol[3*i+2] ;
	}


	for (int i=0; i< Xhs.ndof; ++i){
	  etax[][i] = sol[Vhf.ndof+2*i];
	  etay[][i] = sol[Vhf.ndof+2*i+1];
	}

	//cout  << etay[] << endl;

	wx = (1.+cn)*tau*(etax - etaxold) -cn*wxold;
	wy = (1.+cn)*tau*(etay - etayold) -cn*wyold;



	if ( display == 1) {
	  lift;
	  mesh Thfm=movemesh(Thf,[x+coef*etaFx,y+coef*etaFy]);
	  fespace Qhfm(Thfm,P1);
	  Qhfm pm;
	  for (int i=0; i<Qhfm.ndof; ++i)
	    pm[][i] = p[][i];
	  mesh Thsm=movemesh(Ths,[x+coef*etax,y+coef*etay]);	
	  plot(pm,Thsm,fill=true,value=1, cmm=("time = "+t ) );
	}     
	
	etaxold = etax;
	etayold = etay;	
	wxold1 = wxold;
	wyold1 = wyold;
	wxold = wx;
	wyold = wy;	
	u1old=u1;
	u2old=u2;
 }



 // displacement display via gnuplot
        {
	  string flname = "plot_imp.gp";
	    ofstream gnu(flname);
	  ofstream etadof(flname+"2d");
	  for (int i=0;i<nbint; ++i)
	    gnu << xcs[i] << " " << etay[][matchs[i]] << endl;
	  
	  etadof << h << " " << Xhs.ndof << endl;
	  for (int i=0;i<Xhs.ndof; ++i)
	    etadof <<  etax[][i] << " " << etay[][i] << endl;
	  
        }
