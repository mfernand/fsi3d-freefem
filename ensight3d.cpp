#include <ff++.hpp>
using namespace Fem2D;


  string getPostfix(int n)
  {
    
    std::ostringstream index;
    index << n;
    string postfix;
    switch ( index.str().size() )
      {
      case 1:
	postfix = "0000" + index.str();
	break;
      case 2:
	postfix = "000" + index.str();
	break;
      case 3:
	postfix = "00" + index.str();
	break;
      case 4:
	postfix = "0" + index.str();
	break;
      case 5:
	postfix = index.str();
	break;
      default:
	cout << " error in getPostFix " << endl;
	exit(1);
      }
    return postfix;
  }



int wru(double n) {
  ifstream inu1("./Post/u1");
  ifstream inu2("./Post/u2");
  ifstream inu3("./Post/u3");
  string outf = "./Post/velocity."+getPostfix(n)+".vct";
  ofstream outu(outf.c_str());
  
  int dim;
  inu1 >> dim;
  inu2 >> dim;
  inu3 >> dim;
  int count=0;
  double val[3];
  outu<<"Vector per node"<<std::endl;
  for (int i=0;i<dim;++i){
    inu1 >> val[0];
    inu2 >> val[1];
    inu3 >> val[2];
    for (int j=0; j< 3;++j)
        {
          outu.setf(std::ios_base::scientific); 
          outu.precision(5);    
          outu.width(12);
          outu << val[j];
          ++count;
          if ( count == 6 ) 
            {
              outu << std::endl;
              count=0;
            }
        } 
  }
  outu << std::endl;
  outu.close(); 
  inu1.close();
  inu2.close();
  inu3.close();

  return 0;
}


int wrd(double n) {
  ifstream inu1("./Post/dep1");
  ifstream inu2("./Post/dep2");
  ifstream inu3("./Post/dep3");
  string outf = "./Post/displacement."+getPostfix(n)+".vct";
  ofstream outu(outf.c_str());
  
  int dim;
  inu1 >> dim;
  inu2 >> dim;
  inu3 >> dim;
  int count=0;
  double val[3];
  outu<<"Vector per node"<<std::endl;
  for (int i=0;i<dim;++i){
    inu1 >> val[0];
    inu2 >> val[1];
    inu3 >> val[2];
    for (int j=0; j< 3;++j)
        {
          outu.setf(std::ios_base::scientific); 
          outu.precision(5);    
          outu.width(12);
          outu << val[j];
          ++count;
          if ( count == 6 ) 
            {
              outu << std::endl;
              count=0;
            }
        } 
  }
  outu << std::endl;
  outu.close(); 
  inu1.close();
  inu2.close();
  inu3.close();
  return 0;
}



int wrp(double j) {
  ifstream inp("./Post/p");
  string outf = "./Post/pressure."+getPostfix(j)+".scl";
  ofstream outp(outf.c_str());
  
  int dim;
  inp >> dim;
  
  int count=0;
  double val;
  outp <<"Scalar per node"<< endl;
  for (int i=0;i<dim;++i) 
      {
	inp >> val;
        outp.setf(std::ios_base::scientific);   
        outp.precision(5);      
        outp.width(12);
        outp << val;
        ++count;
        if ( count == 6 ) 
          {
            outp << endl;
            count=0;
          }
      }
  outp << endl;
  outp.close();
  inp.close();
  
  return 0;
}


int wrm(double j) {
  ifstream inm("./Post/mesh");
  ofstream geof("./Post/fluid.geo");
  int nV, nE, nBF;
  inm >> nV;
  inm >> nE;
  inm >> nBF;

  int part=0;

  geof<<"Geometry file"<<std::endl;
  geof<<"Geometry file"<<std::endl;
  geof<<"node id assign"<<std::endl;
  geof<<"element id assign"<<std::endl;
  geof<< "coordinates" << std::endl;
  geof.width(8);
  geof<< nV << std::endl;

  double coor[3];
  for(int i=0; i < nV; ++i) 
    {
      inm >> coor[0] >> coor[1]>> coor[2];
      for (int j=0; j < 3; ++j) 
	{
	  geof.setf(std::ios_base::scientific);
	  geof.precision(5);  
	  geof.width(12);
	  geof <<  coor[j];
	}
      
      geof<< std::endl;
    } 


    // volume parts
    std::set<int> flags;
    std::map< int, list<int> > geo;

    typedef std::set<int>::const_iterator Iterator_flag;
    typedef list<int>::const_iterator Iterator_geo;
    Iterator_flag result;
    int marker;
    vector< vector<int> > con(nE);

    for (int i=0; i < nE; ++i)
      {
	con[i].resize(4);
	inm >> con[i][0] >> con[i][1] >> con[i][2] >> con[i][3] >>marker;
        flags.insert(marker);
        geo[marker].push_back(i);
      }
    for (Iterator_flag i=flags.begin(); i!= flags.end(); ++i)
      {
        marker = *i;
        geof<< "part ";
        geof.width(8);
        ++part;
        geof<< part << std::endl;
        geof<<"subdomain ref "<< marker << std::endl;
        list<int>& volumeList= geo[marker];
        geof<< "tetra4" << std::endl;
        geof.width(8);
        geof<< volumeList.size() << std::endl;
        for (Iterator_geo j=volumeList.begin(); j!= volumeList.end(); ++j) 
          {
            for(int k=0; k < 4; ++k) 
              {
                geof.width(8);
                geof << con[*j][k]+1;
              }
	    con[*j].clear();
            geof<<"\n";
          }
      }
    
    geo.clear();
    flags.clear();

    // boundary parts
       
    con.resize(nBF);
    for (int i=0; i < nBF; ++i)
      {
	con[i].resize(3);
	inm >> con[i][0] >> con[i][1]  >>con[i][2] >>  marker;
        flags.insert(marker);
        geo[marker].push_back(i);
      }
      
    for (Iterator_flag i=flags.begin(); i!= flags.end(); ++i)
      {
        marker = *i;
        geof<< "part";
        geof.width(8);
        ++part;
        geof<< part << std::endl;
        geof<<"boundary ref "<< marker << std::endl;
        list<int>& faceList= geo[marker];
        geof<< "tria3" << std::endl;
        geof.width(8);
        geof<< faceList.size() << std::endl;
        for (Iterator_geo j=faceList.begin(); j!= faceList.end(); ++j) 
          {
            for(int k=0; k < 3; ++k) 
              {
                geof.width(8);
                geof << con[*j][k]+1;
              }
            geof<<"\n";
          }
      }
     
    geo.clear();
    flags.clear();
    geof.close();
    return 0;
}

int wrms(double j) {
  ifstream inm("./Post/meshs");
  ofstream geof("./Post/structure.geo");

  int nV, nE, nBF;
  inm >> nV;
  inm >> nE;
  inm >> nBF;

  int part=0;

  geof<<"Geometry file"<<std::endl;
  geof<<"Geometry file"<<std::endl;
  geof<<"node id assign"<<std::endl;
  geof<<"element id assign"<<std::endl;
  geof<< "coordinates" << std::endl;
  geof.width(8);
  geof<< nV << std::endl;

  double coor[3];
  for(int i=0; i < nV; ++i) 
    {
      inm >> coor[0] >> coor[1]>> coor[2];
      for (int j=0; j < 3; ++j) 
	{
	  geof.setf(std::ios_base::scientific);
	  geof.precision(5);  
	  geof.width(12);
	  geof <<  coor[j];
	}
      
      geof<< std::endl;
    } 


    // volume parts
    std::set<int> flags;
    std::map< int, list<int> > geo;

    typedef std::set<int>::const_iterator Iterator_flag;
    typedef list<int>::const_iterator Iterator_geo;
    Iterator_flag result;
    int marker;
    vector< vector<int> > con(nE);

    for (int i=0; i < nE; ++i)
      {
	con[i].resize(4);
	inm >> con[i][0] >> con[i][1] >> con[i][2] >> con[i][3] >>marker;
        flags.insert(marker);
        geo[marker].push_back(i);
      }
    for (Iterator_flag i=flags.begin(); i!= flags.end(); ++i)
      {
        marker = *i;
        geof<< "part ";
        geof.width(8);
        ++part;
        geof<< part << std::endl;
        geof<<"subdomain ref "<< marker << std::endl;
        list<int>& volumeList= geo[marker];
        geof<< "tetra4" << std::endl;
        geof.width(8);
        geof<< volumeList.size() << std::endl;
        for (Iterator_geo j=volumeList.begin(); j!= volumeList.end(); ++j) 
          {
            for(int k=0; k < 4; ++k) 
              {
                geof.width(8);
                geof << con[*j][k]+1;
              }
	    con[*j].clear();
            geof<<"\n";
          }
      }
    
    geo.clear();
    flags.clear();

    // boundary parts
       
    con.resize(nBF);
    for (int i=0; i < nBF; ++i)
      {
	con[i].resize(3);
	inm >> con[i][0] >> con[i][1]  >>con[i][2] >>  marker;
        flags.insert(marker);
        geo[marker].push_back(i);
      }
      
    for (Iterator_flag i=flags.begin(); i!= flags.end(); ++i)
      {
        marker = *i;
        geof<< "part";
        geof.width(8);
        ++part;
        geof<< part << std::endl;
        geof<<"boundary ref "<< marker << std::endl;
        list<int>& faceList= geo[marker];
        geof<< "tria3" << std::endl;
        geof.width(8);
        geof<< faceList.size() << std::endl;
        for (Iterator_geo j=faceList.begin(); j!= faceList.end(); ++j) 
          {
            for(int k=0; k < 3; ++k) 
              {
                geof.width(8);
                geof << con[*j][k]+1;
              }
            geof<<"\n";
          }
      }
     
    geo.clear();
    flags.clear();
    geof.close();
    return 0;
  
}

int wrc(double tps) {
  static vector<double> time;
  time.push_back(tps);
  ofstream casef("./Post/fluid.case");
  casef << "FORMAT\n";
  casef << "type: ensight\n";
  casef << "GEOMETRY\n";
  casef << "model: 1 fluid.geo\n";
  casef << "VARIABLE\n";
  casef << "scalar per node: 1 pressure pressure.*****.scl\n";
  casef << "vector per node: 1 velocity velocity.*****.vct\n";
  casef << "TIME\n";
  casef << "time set: 1\n"; 
  casef << "number of steps: " <<  time.size() << "\n";
  casef << "filename start number: 0\n";
  casef << "filename increment: 1\n";
  casef << "time values:\n";
  int count=0;
  
  
  for (int i =0; i < time.size(); ++i)
    {
      casef << time[i] << " " ;    
      ++count;
      if ( count == 6) 
	{
	  casef <<"\n";
	  count = 0;
	}
    }
  
  casef.close();
}


int wrcs(double tps) {
  static vector<double> time;
  time.push_back(tps);
  ofstream casef("./Post/structure.case");
  casef << "FORMAT\n";
  casef << "type: ensight\n";
  casef << "GEOMETRY\n";
  casef << "model: 1 structure.geo\n";
  casef << "VARIABLE\n";
  casef << "vector per node: 1 displacement displacement.*****.vct\n";
  casef << "TIME\n";
  casef << "time set: 1\n"; 
  casef << "number of steps: " <<  time.size() << "\n";
  casef << "filename start number: 0\n";
  casef << "filename increment: 1\n";
  casef << "time values:\n";
  int count=0;
  
  
  for (int i =0; i < time.size(); ++i)
    {
      casef << time[i] << " " ;    
      ++count;
      if ( count == 6) 
	{
	  casef <<"\n";
	  count = 0;
	}
	}
  
  casef.close();
}


class Init { public:
  Init();
};

LOADFUNC(Init);
Init::Init(){
  //Global.Add("writemesh","(",new OneOperator1<int,int>(writemesh));
  Global.Add("writep","(",new OneOperator1<int,double>(wrp));
  Global.Add("writeu","(",new OneOperator1<int,double>(wru));
  Global.Add("writed","(",new OneOperator1<int,double>(wrd));
  Global.Add("writem","(",new OneOperator1<int,double>(wrm));
  Global.Add("writems","(",new OneOperator1<int,double>(wrms));
  Global.Add("writec","(",new OneOperator1<int,double>(wrc));
  Global.Add("writecs","(",new OneOperator1<int,double>(wrcs));
}
